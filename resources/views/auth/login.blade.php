

<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>{{$title}}</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<link href="{{asset('assets/dashboard/css/vendor.min.css')}}" rel="stylesheet" />
	<link href="{{asset('assets/dashboard/css/facebook/app.min.css')}}" rel="stylesheet" />
	<!-- ================== END core-css ================== -->
</head>
<body class='pace-top'>
	<!-- BEGIN #loader -->
	
	<div id="app" class="app">
		<!-- BEGIN register -->
		<div class="register register-with-news-feed">
			<!-- BEGIN news-feed -->
			<div class="news-feed">
				<div class="news-image" style="background-image: url({{asset('storage/' . App\Models\Content::where('name', 'slider_background')->first()->description)}})"></div>
				<div class="news-caption">
					<h4 class="caption-title">{{App\Models\Content::where('name', 'name')->first()->description}}</h4>
					
				</div>
			</div>
			<!-- END news-feed -->
			
			<!-- BEGIN register-container -->
			<div class="register-container">
				<!-- BEGIN register-header -->
				<div class="register-header mb-25px h1">
					<div class="mb-1">Sign In</div>
				</div>
				<!-- END register-header -->
				
				<!-- BEGIN register-content -->
				<div class="register-content">
                    @if (session()->has('error'))
                      <div class="alert alert-danger" role="alert">
                        {{session('error')}}
                      </div>
                    @endif
					<form action="/auth/login" method="POST" class="fs-13px">
						
                        @csrf
						<div class="mb-3">
							<label class="mb-2">Email <span class="text-danger">*</span></label>
							<input type="email" name="email" value="{{ old('email') }}" class="form-control fs-13px" placeholder="Email address" />
                            @error('email')
                                {{$message}}
                            @enderror
						</div>
					
						<div class="mb-4">
							<label class="mb-2">Password <span class="text-danger">*</span></label>
							<input type="password" name="password" class="form-control fs-13px" placeholder="Password" />
                            @error('password')
                                {{$message}}
                            @enderror
						</div>
					
						<div class="mb-4">
							<button type="submit" class="btn btn-primary d-block w-100 btn-lg h-45px fs-13px">Sign Up</button>
						</div>
					</form>
				</div>
				<!-- END register-content -->
			</div>
			<!-- END register-container -->
		</div>
	</div>
	
</body>
</html>