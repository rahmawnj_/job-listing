                        <span> {{ $count_job }} Jobs found</span>
 
                        @foreach ($jobs as $job)
                     
                        <div class="single-job-items mb-30" style="border: 1px solid #8ed9f8;">
                            <div class="job-items align-items-center">
                                <div class="company-img">
                                    <a href="/job/{{$job->id}}"><img height="60" style="border: 1px #c4bfbf solid" src="{{asset('storage/' . $job->image)}}" alt=""></a>
                                </div>
                                <div class="job-tittle job-tittle2">
                                    <a href="/job/{{$job->id}}">
                                        <h4>{{$job->title}}</h4>
                                    </a>
                                    <ul>
                                        <li >{{\App\Models\Jobcategory::where('id', $job->jobcategory_id)->first()->name}}</li>
                                        <li ><i class="fas fa-map-marker-alt"></i>{{\App\Models\Location::where('id', $job->location_id)->first()->name}}</li>
                                       
                                    </ul>
                                     <li >{{$job->salary}}</li>
                                </div>
                            </div>
                        <div class="items-link items-link2 f-right">
    @if($job->updated_at > $job->created_at)
        <a href="/job/{{$job->id}}">{{ucwords(str_replace("_", " ", $job->type))}}</a>
        <span>{{\Carbon\Carbon::parse($job->updated_at)->diffForHumans()}}</span>
    @else
        <a href="/job/{{$job->id}}">{{ucwords(str_replace("_", " ", $job->type))}}</a>
        <span>{{\Carbon\Carbon::parse($job->created_at)->diffForHumans()}}</span>
    @endif
</div>

                        </div>
                        @endforeach
                    
                        <div class="pagination-area pb-115 text-center">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="single-wrap d-flex justify-content-center">
                                            <nav aria-label="Page navigation example">
                                                {{-- {{$jobs->links()}} --}}
                                                @if ($jobs->hasPages())
                                                <ul class="pagination" role="navigation">
                                                    {{-- Previous Page Link --}}
                                                    @if ($jobs->onFirstPage())
                                                        <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                                                            <span class="page-link" aria-hidden="true">&lsaquo;</span>
                                                        </li>
                                                    @else
                                                        <li class="page-item">
                                                            <a class="page-link" href="{{ $jobs->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                                                        </li>
                                                    @endif
                                                
                                                    <?php
                                                        $start = $jobs->currentPage() ; // show 3 pagination links before current
                                                        $end = $jobs->currentPage(); // show 3 pagination links after current
                                                        if($start < 1) {
                                                            $start = 1; // reset start to 1
                                                            $end += 1;
                                                        } 
                                                        if($end >= $jobs->lastPage() ) $end = $jobs->lastPage(); // reset end to last page
                                                    ?>
                                                
                                                    @if($start > 1)
                                                        <li class="page-item">
                                                            <a class="page-link" href="{{ $jobs->url(1) }}">{{1}}</a>
                                                        </li>
                                                        @if($jobs->currentPage() -1 != 1)
                                                            {{-- "Three Dots" Separator --}}
                                                            <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>
                                                        @endif
                                                    @endif
                                                        @for ($i = $start; $i <= $end; $i++)
                                                            <li class="page-item {{ ($jobs->currentPage() == $i) ? ' active' : '' }}">
                                                                <a class="page-link" href="{{ $jobs->url($i) }}">{{$i}}</a>
                                                            </li>
                                                        @endfor
                                                    @if($end < $jobs->lastPage())
                                                        @if($jobs->currentPage() + 1 != $jobs->lastPage())
                                                            {{-- "Three Dots" Separator --}}
                                                            <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>
                                                        @endif
                                                        <li class="page-item">
                                                            <a class="page-link" href="{{ $jobs->url($jobs->lastPage()) }}">{{$jobs->lastPage()}}</a>
                                                        </li>
                                                    @endif
                                                
                                                    {{-- Next Page Link --}}
                                                    @if ($jobs->hasMorePages())
                                                        <li class="page-item">
                                                            <a class="page-link" href="{{ $jobs->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                                                        </li>
                                                    @else
                                                        <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                                                            <span class="page-link" aria-hidden="true">&rsaquo;</span>
                                                        </li>
                                                    @endif
                                                </ul>
                                                @endif
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

