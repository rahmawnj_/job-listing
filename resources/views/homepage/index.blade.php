@extends('homepage._layout.main')

@section('container')

<div class="slider-area ">
    <div class="slider-active"  >
        <div style="" class="single-slider section-overly-hero slider-height d-flex align-items-center" data-background="{{asset('storage/' . App\Models\Content::where('name', 'hero_image')->first()->description)}}">
            <div class="container"  >
                <div class="row mt-auto mb-auto text-center justify-content-center " >
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="hero__caption text-white">
                            <h1 class="text-white">{{\App\Models\Content::where('name', 'hero_description')->first()->description}}</h1>
                            <h2 class="top-0 text-white">{{\App\Models\Content::where('name', 'hero_caption_description')->first()->description}}</h2>
                        </div>
                    </div>
                </div>
                <!-- Search Box -->
                <div  class="row d-flex justify-content-center mt-5">
                    <div  class="col-12"> 
                        <div class="d-flex justify-content-center">
                            <form class="" action="/jobs" method="post">
                                @csrf
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control" name="search" placeholder="Job Tittle or keyword" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                                  <div class="input-group-prepend">
                                    <button style="background-color: #2a93d5; color:white;" type="submit" class="input-group-text" id="inputGroup-sizing-lg"><i class="fa fa-search"></i>  Find Job</button>
                                  </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="support-company-area support-padding fix mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center">
                    <h2 style="color: #1e214e; text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;">About Us</h2>
                </div>
                <div class="support-caption">
                    <div class="mt-5 mb-5">
                        {!! Str::limit(\App\Models\Content::where('name', 'about')->first()->description, 300) !!}
                    </div>
                    <div class="d-flex text-center justify-content-center">
                        <a style="background-color:#2a93d5 " href="/about" class="btn post-btn">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="our-services section-pad-t30">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center">
                    <h2 style="color: #1e214e; text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;">Top Categories</h2>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <div class="single-services text-center mb-30">
                    <div class="services-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Topcategory::where('id', '1')->first()->logo)}}" alt="">
                    </div>
                    <div class="services-cap">
                        <h5><a href="/jobs">{{App\Models\Topcategory::where('id', '1')->first()->title}}</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <div class="single-services text-center mb-30">
                    <div class="services-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Topcategory::where('id', '2')->first()->logo)}}" alt="">
                    </div>
                    <div class="services-cap">
                        <h5><a href="/jobs">{{App\Models\Topcategory::where('id', '2')->first()->title}}</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <div class="single-services text-center mb-30">
                    <div class="services-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Topcategory::where('id', '3')->first()->logo)}}" alt="">
                    </div>
                    <div class="services-cap">
                        <h5><a href="/jobs">{{App\Models\Topcategory::where('id', '3')->first()->title}}</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                <div class="single-services text-center mb-30">
                    <div class="services-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Topcategory::where('id', '4')->first()->logo)}}" alt="">
                    </div>
                    <div class="services-cap">
                        <h5><a href="/jobs">{{App\Models\Topcategory::where('id', '4')->first()->title}}</a></h5>
                    </div>
                </div>
            </div>

        </div>
       
    </div>
</div>

<section class="featured-job-area feature-padding" style="padding-top: 0px;">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center">
                    <h2 style="color: #1e214e; text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;">We are Hiring!</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                        <div class="small-section-tittle2">
                            <h4>Job Category</h4>
                      </div>
                       <!-- Select job items start -->
                       <div class="select-job-items2">
                        <select id="job_category" name="select">
                            <option value="">All Category</option>
                            @foreach (App\Models\Jobcategory::all() as $jobcategory)
                            <option value="{{$jobcategory->id}}">{{$jobcategory->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="small-section-tittle2">
                            <h4>Job Location</h4>
                      </div>
                       <!-- Select job items start -->
                       <select id="location" name="select">
                        <option value="">All Locations</option>
                        @foreach (App\Models\Location::all() as $location)
                        <option value="{{$location->id}}">{{$location->name}}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="col-sm-12 col-lg-4">
                        <div class="small-section-tittle2">
                            <h4>Job Type</h4>
                      </div>
                       <!-- Select job items start -->
                       <select class="form-control" name="job_type" id="job_type">
                        <option value="">All Type</option>
                        <option value="full_time">Full Time</option>
                        <option value="part_time">Part Time</option>
                        <option value="remote">Remote</option>
                        <option value="freelance">Freelance</option>
                    </select>
                    </div>
                </div>

                <div id="job_data">
                    @include('homepage.job_data')
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Featured_job_end -->
<!-- How  Apply Process Start-->
<div class="apply-process-area apply-bg" >
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle white-text text-center">
                    <h2 class="text-dark" style="text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;"> How it works</h2>
                </div>
            </div>
        </div>
        <!-- Apply Process Caption -->
        <div class="row">
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="single-process text-center mb-30" style="background-color: #1e214e;">
                    <div class="process-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '1')->first()->logo)}}" alt="">
                    </div>
                    <div class="process-cap">
                       <h5>{{App\Models\Applyprocess::where('id', '1')->first()->title}}</h5>
                       <p class="text-white">{{App\Models\Applyprocess::where('id', '1')->first()->description}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="single-process text-center mb-30" style="background-color: #1e214e;">
                    <div class="process-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '2')->first()->logo)}}" alt="">
                    </div>
                    <div class="process-cap">
                       <h5>{{App\Models\Applyprocess::where('id', '2')->first()->title}}</h5>
                       <p class="text-white">{{App\Models\Applyprocess::where('id', '2')->first()->description}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="single-process text-center mb-30" style="background-color: #1e214e;">
                    <div class="process-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '3')->first()->logo)}}" alt="">
                    </div>
                    <div class="process-cap">
                       <h5>{{App\Models\Applyprocess::where('id', '3')->first()->title}}</h5>
                       <p class="text-white">{{App\Models\Applyprocess::where('id', '3')->first()->description}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="single-process text-center mb-30" style="background-color: #1e214e;">
                    <div class="process-ion">
                        <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '4')->first()->logo)}}" alt="">
                    </div>
                    <div class="process-cap">
                       <h5>{{App\Models\Applyprocess::where('id', '4')->first()->title}}</h5>
                       <p class="text-white">{{App\Models\Applyprocess::where('id', '4')->first()->description}}</p>
                    </div>
                </div>
            </div>
        </div>
     </div>
</div>

<style>
.section-header h3 {
    font-size: 36px;
    color: #283d50;
    text-align: center;
    font-weight: 500;
    position: relative;
}
.section-header p {
    text-align: center;
    margin: auto;
    font-size: 15px;
    padding-bottom: 60px;
    color: #556877;
    width: 50%;
}
#clients {
    padding: 60px 0;
}
#clients .clients-wrap {
    margin-bottom: 30px;
}
#clients .client-logo {
    padding: 34px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
   
    overflow: hidden;
    background: #fff;
    height: 140px;
}
#clients img {
    transition: all 0.4s ease-in-out;
}
</style>
{{-- 
<section  id="clients" class="section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle white-text text-center">
                    <h2 class="text-dark" style="text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;"> Our Clients</h2>
                </div>
            </div>
        </div>
      <div class="row no-gutters clients-wrap clearfix wow d-flex justify-content-center" style="visibility: visible;">
        @foreach (\App\Models\Company::where('show', 'active')->get() as $company)
        <div style="" class="col-lg-3 col-md-4 col-xs-6 m-2 d-flex justify-content-center">
            <div class="client-logo">
              <img  src="{{asset('storage/' . $company->logo)}}" class="img-fluid" alt="">
            </div>
          </div>
        @endforeach
      </div>
    </div>
</section> --}}

<section  id="clients" class="section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle white-text text-center">
                    <h2 class="text-dark" style="text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;"> Our Clients</h2>
                </div>
            </div>
        </div>
      <div class="row no-gutters clients-wrap clearfix wow d-flex justify-content-center" style="visibility: visible;">
        <div class="row d-flex justify-content-center">
        @foreach (\App\Models\Company::where('show', 'active')->get() as $company)
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 my-2 d-flex justify-content-center">
            <img src="{{asset('storage/' . $company->logo)}}" class="img-fluid mx-auto d-block" alt="Client Logo" style="object-fit: cover;">
          </div>
            @endforeach
        </div>
      </div>
    </div>
</section>

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function(event) {
          event.preventDefault();
          var page = $(this).attr('href').split('page=')[1];
          getMoreJobs(page);
        });
        $('#job_type').on('change', function (e) {
					getMoreJobs();
                });
        $('#job_category').on('change', function (e) {
					getMoreJobs();
                });
        });
        $('#location').on('change', function (e) {
					getMoreJobs();
        });

    function getMoreJobs(page) {
      var search = $('#search').val();
      var selectedJobType = $("#job_type option:selected").val();
      var selectedJobCategory = $("#job_category option:selected").val();
      var selectedLocation = $("#location option:selected").val();
      $.ajax({
        type: "GET",
        data: {
          'location': selectedLocation,
          'job_category': selectedJobCategory,
          'job_type' : selectedJobType,
          'search' : search,
        },
        url: "{{ route('jobs.get-more-jobs') }}" + "?page=" + page,
        success:function(data) {
            console.log('data', data)
          $('#job_data').html(data);
        },
        error:function(e){
            console.log(e)
        }
      });
    }
  </script>

@endpush