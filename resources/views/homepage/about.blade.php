@extends('homepage._layout.main')

@push('meta')
<meta name="description" content="{!! \App\Models\Content::where('name', 'about')->first()->description !!}">
@endpush

@section('container')
    <!-- Hero Area Start-->
    <div class="slider-area ">
    <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{asset('storage/' . App\Models\Content::where('name', 'slider_background')->first()->description)}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>{!! \App\Models\Content::where('name', 'title_about_hero')->first()->description !!}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Hero Area End -->
    <!-- Support Company Start-->
 
    <div class="  ">
        <div class="container mt-5">
            <div class="row align-items-center">
                <div class="col-12 d-flex justify-content-center">
                    <div class="" style="margin-top: 50px; margin-top: 10px">
                        <img src="{{asset('storage/' . \App\Models\Content::where('name', 'about_image')->first()->description)}}" height="200" alt="image" class="img-responsive">
                    </div>
                </div>
                <div class="col-12">
                    <div class="right-caption">
                        <div class="">
                            <p class="">{!! \App\Models\Content::where('name', 'about')->first()->description !!}</p>
                        </div>
                    </div>
                </div>
           
            </div>
        </div>
    </div>
    
    <!-- Support Company End-->
    <!-- How  Apply Process Start-->
    <div class="apply-process-area apply-bg">
        <div class="container">
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle text-center">
                        <h2 class="text-dark" style="text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;"> How it works</h2>
                    </div>
                </div>
            </div>
            <!-- Apply Process Caption -->
            <div class="row">
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="single-process text-center mb-30">
                        <div class="process-ion">
                            <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '1')->first()->logo)}}" alt="">
                        </div>
                        <div class="process-cap">
                           <h5>{{App\Models\Applyprocess::where('id', '1')->first()->title}}</h5>
                           <p>{{App\Models\Applyprocess::where('id', '1')->first()->description}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="single-process text-center mb-30">
                        <div class="process-ion">
                            <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '2')->first()->logo)}}" alt="">
    
                        </div>
                        <div class="process-cap">
                           <h5>{{App\Models\Applyprocess::where('id', '2')->first()->title}}</h5>
                           <p>{{App\Models\Applyprocess::where('id', '2')->first()->description}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="single-process text-center mb-30">
                        <div class="process-ion">
                            <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '3')->first()->logo)}}" alt="">
                        </div>
                        <div class="process-cap">
                           <h5>{{App\Models\Applyprocess::where('id', '3')->first()->title}}</h5>
                           <p>{{App\Models\Applyprocess::where('id', '3')->first()->description}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="single-process text-center mb-30">
                        <div class="process-ion">
                            <img height="70" src="{{asset('storage/' . App\Models\Applyprocess::where('id', '4')->first()->logo)}}" alt="">
                        </div>
                        <div class="process-cap">
                           <h5>{{App\Models\Applyprocess::where('id', '4')->first()->title}}</h5>
                           <p>{{App\Models\Applyprocess::where('id', '4')->first()->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
   
    <style>
        .section-header h3 {
            font-size: 36px;
            color: #283d50;
            text-align: center;
            font-weight: 500;
            position: relative;
        }
    
        .section-header p {
            text-align: center;
            margin: auto;
            font-size: 15px;
            padding-bottom: 60px;
            color: #556877;
            width: 50%;
        }
    
        #clients {
            padding: 60px 0;
            
        }
        #clients .clients-wrap {
            margin-bottom: 30px;
        }
    </style>
   <section  id="clients" class="section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle white-text text-center">
                    <h2 class="text-dark" style="text-decoration: underline; text-decoration-color: #8ed9f8; text-decoration-thickness:2.5px;"> Our Clients</h2>
                </div>
            </div>
        </div>
      <div class="row no-gutters clients-wrap clearfix wow d-flex justify-content-center" style="visibility: visible;">
        <div class="row d-flex justify-content-center">
        @foreach (\App\Models\Company::where('show', 'active')->get() as $company)
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 my-2 d-flex justify-content-center">
            <img src="{{asset('storage/' . $company->logo)}}" class="img-fluid mx-auto d-block" alt="Client Logo" style="object-fit: cover;">
          </div>
            @endforeach
        </div>
      </div>
    </div>
</section>

@endsection
