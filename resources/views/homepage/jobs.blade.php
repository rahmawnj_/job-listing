@extends('homepage._layout.main')

@section('container')

<div class="slider-area ">
    <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{asset('storage/' . App\Models\Content::where('name', 'slider_background')->first()->description)}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>{!! \App\Models\Content::where('name', 'title_jobs_hero')->first()->description !!}</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hero Area End -->
<!-- Job List Area Start -->
<div class="job-listing-area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <!-- Left content -->
            <div class="col-xl-3 col-lg-3 col-md-4">
                <div class="row">
                    <div class="col-12">
                            <div class="small-section-tittle2 mb-45">
                            <div class="ion"> <svg 
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="20px" height="12px">
                            <path fill-rule="evenodd"  fill="rgb(27, 207, 107)"
                                d="M7.778,12.000 L12.222,12.000 L12.222,10.000 L7.778,10.000 L7.778,12.000 ZM-0.000,-0.000 L-0.000,2.000 L20.000,2.000 L20.000,-0.000 L-0.000,-0.000 ZM3.333,7.000 L16.667,7.000 L16.667,5.000 L3.333,5.000 L3.333,7.000 Z"/>
                            </svg>
                            </div>
                            <h4>Filter Jobs</h4>
                        </div>
                    </div>
                </div>
                <!-- Job Category Listing start -->
                <div class="job-category-listing mb-50">
                    <form action="">
                    <div class="single-listing">
                        <div class="select-Categories pt-40 pb-50">
                            <input class="form-control" name="search" id="search" type="search" placeholder="Search...">
                        </div>
                        <!-- select-Categories End -->
                    </div>
                    <div class="single-listing">
                        <div class="small-section-tittle2">
                            <h4>Job Location</h4>
                      </div>
                       <div class="select-job-items2">
                           <select id="location" name="select">
                               <option value="">All Locations</option>
                               @foreach (App\Models\Location::all() as $location)
                               <option value="{{$location->id}}">{{$location->name}}</option>
                               @endforeach
                           </select>
                       </div>
                    </div>
                    <div class="single-listing">
                       

                       <div class="small-section-tittle2">
                             <h4>Job Category</h4>
                       </div>
                        <div class="select-job-items2">
                            <select id="job_category" name="select">
                                <option value="">All Category</option>
                                @foreach (App\Models\Jobcategory::all() as $jobcategory)
                                <option value="{{$jobcategory->id}}">{{$jobcategory->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="select-Categories pt-80 pb-50">
                            <div class="small-section-tittle2">
                                <h4>Job Type</h4>
                            </div>
                            <label class="container">Full Time
                                <input name="job_type" type="radio" value="full_time">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">Part Time
                                <input name="job_type" type="radio" value="part_time">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">Remote
                                <input name="job_type"  type="radio" value="remote">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">Freelance
                                <input name="job_type" type="radio" value="freelance">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <!-- select-Categories End -->
                    </div>
                 
                </form>

                </div>
                <!-- Job Category Listing End -->
            </div>
            <!-- Right content -->
            <div class="col-xl-9 col-lg-9 col-md-8">
                <!-- Featured_job_start -->
                <section class="featured-job-area">
                    <div class="container">
                        <!-- Count of Job list Start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="count-job mb-35">
                                    {{-- <span>{{App\Models\Job::all()->count()}} Jobs found</span> --}}
                                    <!-- Select job items start -->
                                    <div class="select-job-items">
                                        <span>Sort by</span>
                                        <select id="sort_by" name="select">
                                            <option value="">None</option>
                                            <option value="latest">Latest</option>
                                            <option value="oldest">Oldest</option>
                                        </select>
                                    </div>
                                    <!--  Select job items End-->
                                </div>
                            </div>
                        </div>
                        <div id="job_data">
                            @include('homepage.job_data')
                        </div>
                    </div>
                </section>
                <!-- Featured_job_end -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function(event) {
          event.preventDefault();
          var page = $(this).attr('href').split('page=')[1];
          getMoreJobs(page);
        });


        $('#search').on('keyup', function() {
          $value = $(this).val();
          getMoreJobs(1);
        });

        $('#sort_by').on('change', function (e) {
					getMoreJobs();
        });
        
        $('[name="job_type"]').on('change', function (e) {
					getMoreJobs();
                });
        $('#job_category').on('change', function (e) {
					getMoreJobs();
                });
        $('#location').on('change', function (e) {
					getMoreJobs();
                });
        });


    function getMoreJobs(page) {
      var search = $('#search').val();
      var selectedCountry = $("#country option:selected").val();
      var selectedSortBy = $("#sort_by option:selected").val();
      var selectedJobType = $("[name='job_type']:checked").val();
      var selectedJobCategory = $("#job_category option:selected").val();
      var selectedLocation = $("#location option:selected").val();
      $.ajax({
        type: "GET",
        data: {
          'location': selectedLocation,
          'job_category': selectedJobCategory,
          'job_type' : selectedJobType,
          'sort_by' : selectedSortBy,
          'search' : search,
        },
        url: "{{ route('jobs.get-more-jobs') }}" + "?page=" + page,
        success:function(data) {
          $('#job_data').html(data);
        },
        error:function(e){
console.log(e)
        }
      });
    }
  </script>

@endpush