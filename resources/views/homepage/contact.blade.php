@extends('homepage._layout.main')

@push('meta')
<meta name="description" content="{{\App\Models\Content::where('name', 'email')->first()->description}}">
@endpush

@section('container')


<div class="slider-area ">
    <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{asset('storage/' . App\Models\Content::where('name', 'slider_background')->first()->description)}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>{!! \App\Models\Content::where('name', 'title_contact_hero')->first()->description !!}</h2>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Hero Area End -->
<!-- ================ contact section start ================= -->
<section class="contact-section">
        <div class="container">
            <div class="d-none d-sm-block mb-5 pb-4">
            </div>

            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title">Get in Touch</h2>
                </div>
                <div class="col-lg-8">
                    @if (session()->has('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                    @if (session()->has('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                    @endif
                    <form class="form-contact contact_form" action="/send_message" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea required class="form-control w-100" name="message" id="message" cols="30" rows="9"  placeholder=" Enter Message"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input required class="form-control valid" name="name" id="name" type="text"  placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input required class="form-control valid" name="email" id="email" type="email"  placeholder="Email">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input required class="form-control" name="subject" id="subject" type="text"  placeholder="Enter Subject">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1">
               <iframe height="250" width="100%" src="https://maps.google.com/maps?q={{\App\Models\Content::where('name', 'map_location')->first()->description}}&output=embed">hdhkshdjk</iframe>

                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-home"></i></span>
                        <div class="media-body">
                            {{\App\Models\Content::where('name', 'address')->first()->description}}
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                        <div class="media-body">
                            {{\App\Models\Content::where('name', 'phone')->first()->description}}
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body">
                            {{\App\Models\Content::where('name', 'email')->first()->description}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
