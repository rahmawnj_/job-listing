@extends('homepage._layout.main')

@push('meta')
<meta name="description" content="{{\App\Models\Job::find($id)->title}} {{App\Models\Job::find($id)->company->name}}">
@endpush

@section('container')
    <!-- Hero Area Start-->
    <div class="slider-area ">
    <div class="single-slider section-overly slider-height2 d-flex align-items-center" data-background="{{asset('storage/' . App\Models\Content::where('name', 'slider_background')->first()->description)}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2>{{App\Models\Job::find($id)->jobcategory->name}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Hero Area End -->
    <!-- job post company Start -->
    <div class="job-post-company pt-120 pb-120">
        <div class="container">
            <div class="row justify-content-between">
                <!-- Left Content -->
                <div class="col-xl-7 col-lg-8">
                    <!-- job single -->
                    <div class="single-job-items mb-50">
                        <div class="job-items align-items-center">
                            <div class="company-img">
                                <img height="60" style="border: 1px #c4bfbf solid" src="{{asset('storage/' . App\Models\Job::find($id)->image)}}" alt="">
                            </div>
                            <div class="job-tittle">
                                    <h4>{{App\Models\Job::find($id)->title}}</h4>
                                <ul>
                                    <li>{{App\Models\Job::find($id)->jobcategory->name}}</li>
                                    <li><i class="fas fa-map-marker-alt"></i>{{App\Models\Job::find($id)->location->name}}</li>
                                    <li>{{App\Models\Job::find($id)->salary}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                      <!-- job single End -->
                   
                    <div class="job-post-details">
                        <div class="post-details1 mb-50">
                            <!-- Small Section Tittle -->
                            <div class="small-section-tittle">
                                <h4>Job Description</h4>
                            </div>
                           {!! App\Models\Job::find($id)->description!!}
                        </div>
                       
                        <div class="post-details2  mb-50">
                             <!-- Small Section Tittle -->
                            <div class="small-section-tittle">
                                <h4>Requirement, Qualification & Experience</h4>
                            </div>
                            {!! App\Models\Job::find($id)->requirement!!}
                        </div>
                    </div>

                </div>
                <!-- Right Content -->
                <div class="col-xl-4 col-lg-4">
                    <div class="post-details3  mb-50">
                        <!-- Small Section Tittle -->
                       <div class="small-section-tittle">
                           <h4>Job Overview</h4>
                       </div>
                      <ul>
                          <li>Posted date : <span>{{\Carbon\Carbon::parse(App\Models\Job::find($id)->created_at)->format('d-m-Y')}}</span></li>
                          <li>Location : <span>{{App\Models\Job::find($id)->location->name}}</span></li>
                          <li>Total Position : <span>{{App\Models\Job::find($id)->total_position}}</span></li>
                          <li>Job Type : <span>{{ucwords(str_replace("_", " ", App\Models\Job::find($id)->type))}}</span></li>
                          <li>Salary :  <span>{{App\Models\Job::find($id)->salary}}</span></li>
                          {{-- <li>Application date : <span>12 Sep 2020</span></li> --}}
                      </ul>
                     <div class="apply-btn2">
                        <a href="{{  route('apply_job', $id) }}" class="btn">Apply Now</a>
                     </div>
                   </div>
                    {{-- <div class="post-details4  mb-50">
                        <!-- Small Section Tittle -->
                       <div class="small-section-tittle">
                           <h4>Company Information</h4>
                       </div>
                          <span>{{App\Models\Job::find($id)->company->name}}</span>
                          <p>{{App\Models\Job::find($id)->company->description}}</p>
                        <ul>
                            <li>Name: <span>{{App\Models\Job::find($id)->company->name}}</span></li>
                            <li>Web : <span> {{App\Models\Job::find($id)->company->web}}</span></li>
                            <li>Email: <span>{{App\Models\Job::find($id)->company->email}}</span></li>
                        </ul>
                   </div> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- job post company End -->


@endsection
