@extends('dashboard._layout.main')

@section('container')

    <!-- BEGIN breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item active">{{$title}}</li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    <h1 class="page-header">{{$title}}</h1>
    <!-- END page-header -->
    
    <div class="row">
        <!-- BEGIN col-2 -->
     
        <!-- END col-2 -->
        <!-- BEGIN col-10 -->
        <div class="col-xl-12">
            <!-- BEGIN panel -->
            <div class="panel panel-inverse">
                <!-- BEGIN panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">{{$title}}</h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        <a href="/dashboard/jobs/create" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <!-- END panel-heading -->
                <!-- BEGIN alert -->
               
                @if (session()->has('success'))
                <div class="flash-data-success" data-flashdatasuccess="{{session('success')}} " ></div>
                @endif
                @if (session()->has('error'))
                <div class="flash-data-error" data-flashdataerror="{{session('error')}} " ></div>
                @endif
                @if (session()->has('warning'))
                <div class="flash-data-warning" data-flashdatawarning="{{session('warning')}} " ></div>
                @endif
                <!-- END alert -->
                <!-- BEGIN panel-body -->
                <div class="panel-body">
                    <table style="width: 100%;" id="data-table-buttons" class="table table-striped table-bordered align-middle">
                        <thead>
                            <tr>
                                <th width="1%"></th>
                                <th data-orderable="false">Image</th>
                                <th>Job Title</th>
                                <th class="text-nowrap">Company Name</th>
                                <th class="text-nowrap">Job Category</th>
                                <th class="">Apply</th>
                                <th class="text-nowrap"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jobs as $job)
                            <tr class="">
                                <td width="1%" class="fw-bold text-inverse">{{$loop->iteration}}</td>
                                <td width="">
                                    <figure class="m-auto d-flex justify-content-center rounded" style="width:50px; overflow:hidden;" >
                                        @if ($job->image)
                                        <img class="h-50px my-n1 mx-n1" src="{{asset('storage/' . $job->image)}}" id="img-preview" alt="">
                                        @else 
                                        <img class="h-50px my-n1 mx-n1" src="{{asset('img/default.png')}}" id="img-preview" alt="">
                                        @endif
                                    </figure>
                                </td>
                                <td>{{$job->title}}</td>
                                <td>{{$job->company->name}}</td>
                                <td>{{$job->jobcategory->name}}</td>
                                <td> <a href="{{route('admin.job.apply-job', $job->id)}}" class="btn btn-info">{{$job->applyjobs->count()}}</a></td>
                                <td class="">
                                    <a class="btn btn-warning" href="/dashboard/jobs/{{$job->id}}/edit"><i class="fa fa-edit"></i></a>
                                    <a class="d-inline btn btn-info" href="/dashboard/jobs/{{$job->id}}"><i class="fa fa-eye"></i></a>
                                    <form class="d-inline" id="delete-form" action="/dashboard/jobs/{{$job->id}}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button class=" btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- END hljs-wrapper -->
            </div>
            <!-- END panel -->
        </div>
        <!-- END col-10 -->
    </div>
@endsection
