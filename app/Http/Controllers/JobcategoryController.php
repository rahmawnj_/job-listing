<?php

namespace App\Http\Controllers;

use App\Models\Jobcategory;
use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class JobcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Jobcategory::all());
        return view('dashboard.jobcategories.index', [
            'title' => 'Job Categories',
            'jobcategories' => Jobcategory::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.jobcategories.create', [
            'title' => 'Job Categories'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required|unique:jobcategories',
        ]);

        Jobcategory::create([
            'name' => $request->name,
            'slug' => $request->slug,
        ]);
        return redirect('/dashboard/jobcategories')->with('success', 'New Job Category has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Jobcategory $jobcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobcategory $jobcategory)
    {
        return view('dashboard.jobcategories.edit', [
            'title' => 'Job Categories',
            'jobcategory' => $jobcategory
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobcategory $jobcategory)
    {
        if($request->slug == $jobcategory->slug){
            $rules_slug = 'required';
        } else {
            $rules_slug = 'required|unique:jobcategories';
        }
        $request->validate([
            'name' => 'required',
            'slug' => $rules_slug,
        ]);
        Jobcategory::where('id', $jobcategory->id)->update([
            'name' => $request->name,
            'slug' => $request->slug
        ]);
        return redirect('/dashboard/jobcategories')->with('success', 'Job Category has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jobcategory  $jobcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobcategory $jobcategory)
    {
        if (count($jobcategory->jobs) == 0) {
            Jobcategory::destroy($jobcategory->id);
            return redirect('/dashboard/jobcategories')->with('success', 'Job Category has been Deleted');
        } else {
            return redirect('/dashboard/jobcategories')->with('error', 'Delete Job Category failed');
        }
    }

    public function slug(Request $request)
    {
        $slug = SlugService::createSlug(Jobcategory::class, 'slug', $request->name);
        return response()->json(['slug' => $slug]);
    }
}
