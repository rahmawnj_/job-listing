<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.companies.index', [
            'title' => 'Companies',
            'companies' => Company::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.companies.create', [
            'title' => 'Companies'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'website' => 'required',
            'show' => 'required',
            'logo' => 'image|file|max:2048'
        ]);
        $validatedData['email'] = $request->email;
        $validatedData['phone'] = $request->phone;
        $validatedData['address'] = $request->address;
        $validatedData['description'] = $request->description;
        if ($request->file('logo')) {
            $validatedData['logo'] = $request->file('logo')->store('uploads/image/companies');
        }
        Company::create($validatedData);
        return redirect('/dashboard/companies')->with('success', 'New Company has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('dashboard.companies.show', [
            'title' => 'Companies',
            'company' => $company
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('dashboard.companies.edit', [
            'title' => 'Companies',
            'company' => $company
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'website' => 'required',
            'show' => 'required',
            'logo' => 'image|file|max:2048'
        ]);
        $validatedData['email'] = $request->email;
        $validatedData['phone'] = $request->phone;
        $validatedData['address'] = $request->address;
        $validatedData['description'] = $request->description;
        if ($request->file('logo')) {
            if ($company->logo) {
                Storage::delete([$company->logo]);
            } 
            $validatedData['logo'] = $request->file('logo')->store('uploads/image/companies');
        }
        Company::where('id', $company->id)->update($validatedData);
        return redirect('/dashboard/companies')->with('success', 'Company has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        if (count($company->jobs) == 0) {
            if ($company->logo) {
                Storage::delete([$company->logo]);
            }
            Company::destroy($company->id);
            return redirect('/dashboard/companies')->with('success', 'Company has been Deleted');
        } else {
            return redirect('/dashboard/companies')->with('error', 'Delete company failed');
        }
    }
}
