<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Company;
use App\Models\ApplyJob;
use App\Models\Location;
use App\Models\Jobcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Job::all());
        return view('dashboard.jobs.index', [
            'title' => 'Jobs',
            'jobs' => Job::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.jobs.create', [
            'title' => 'Jobs',
            'companies' => Company::all(),
            'locations' => Location::all(),
            'jobcategories' => Jobcategory::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'company_id' => 'required',
            'jobcategory_id' => 'required',
            'location_id' => 'required',
            'type' => 'required',
            'title' => 'required',
            'status' => 'required',
            'image' => 'image|file|max:2048'
        ]);

        $validatedData['company_id'] = $request->company_id;
        $validatedData['location_id'] = $request->location_id;
        $validatedData['type'] = $request->type;
        $validatedData['salary'] = $request->salary;
        $validatedData['title'] = $request->title;
        $validatedData['total_position'] = $request->total_position;
        $validatedData['description'] = $request->description;
        $validatedData['requirement'] = $request->requirement;
        $validatedData['status'] = $request->status;

        if ($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('uploads/image/jobs');
        }

        Job::create($validatedData);
        return redirect('/dashboard/jobs')->with('success', 'New Job has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        return view('dashboard.jobs.show', [
            'title' => 'Jobs',
            'job' => $job,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        return view('dashboard.jobs.edit', [
            'title' => 'Jobs',
            'job' => $job,
            'companies' => Company::all(),
            'locations' => Location::all(),
            'jobcategories' => Jobcategory::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        $request->validate([
            'company_id' => 'required',
            'jobcategory_id' => 'required',
            'location_id' => 'required',
            'title' => 'required',
            'type' => 'required',
            'status' => 'required',
            'logo' => 'image|file|max:2048'
        ]);
        if ($request->file('image')) {
            if ($job->image) {
                Storage::delete([$job->image]);
            }
            $validatedData['image'] = $request->file('image')->store('uploads/image/jobs');
        }
        $validatedData['company_id'] = $request->company_id;
        $validatedData['jobcategory_id'] = $request->jobcategory_id;
        $validatedData['location_id'] = $request->location_id;
        $validatedData['title'] = $request->title;
        $validatedData['type'] = $request->type;
        $validatedData['total_position'] = $request->total_position;
        $validatedData['salary'] = $request->salary;
        $validatedData['description'] = $request->description;
        $validatedData['requirement'] = $request->requirement;
        $validatedData['status'] = $request->status;

        Job::where('id', $job->id)->update($validatedData);
        return redirect('/dashboard/jobs')->with('success', 'Job has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        Storage::delete([$job->image]);

        Job::destroy($job->id);
        return redirect('/dashboard/jobs')->with('success', 'Job has been Deleted');
    }

    public function dump()
    {
        return view('dashboard.jobs.dump', [
            'title' => 'Jobs Dump',
            'jobs' => Job::onlyTrashed()->get()
        ]);
    }

    public function dump_destroy($id)
    {
        Job::withTrashed()->find($id)->forceDelete();
        return redirect('/dashboard/jobs/dump')->with('success', 'Job has been Deleted');
    }

    public function apply_jobs(Job $job)
    {
       $applyjobs =  ApplyJob::where('job_id', $job->id)->orderBy('created_at', 'desc')->get();

        return view('dashboard.jobs.apply', [
            'title' => 'Jobs',
            'job' => $job,
            'applyjobs' => $applyjobs,
        ]);
    }

    public function apply_job_detail($apply_job_id)
    {
        $apply_job = ApplyJob::findOrFail($apply_job_id);

        // Ubah status read menjadi 1
        $apply_job->update(['read' => 1]);

        $title = 'Jobs';
        return view('dashboard.jobs.apply_detail', compact('apply_job', 'title'));
    }


public function update_apply_job_detail(Request $request, ApplyJob $apply_job)
{
    $request->validate([
        // 'note' => 'required|string',
        'status' => 'required|in:pending,approved,rejected',
    ]);

    $apply_job->update([
        'note' => $request->note,
        'status' => $request->status,
    ]);

    return redirect()->back()->with('success', 'Catatan dan status lamaran berhasil diperbarui.');
}


}
